module "tfconfig-functions" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/common-functions/tfconfig-functions/tfconfig-functions.sentinel"
}

module "tfplan-functions" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/common-functions/tfplan-functions/tfplan-functions.sentinel"
}

module "aws-functions" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/aws/aws-functions/aws-functions.sentinel"
}



policy "enforce-module-versions" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/cloud-agnostic/require-all-modules-have-version-constraint.sentinel"
    enforcement_level = "hard-mandatory"
}

policy "use-latest-module-versions" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/cloud-agnostic/http-examples/use-latest-module-versions.sentinel"
    enforcement_level = "advisory"
}

policy "enforce-mandatory-tags" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/aws/enforce-mandatory-tags.sentinel"
    enforcement_level = "advisory"
}


policy "require-modules" {
    source = "https://raw.githubusercontent.com/hashicorp/terraform-guides/master/governance/third-generation/cloud-agnostic/require-all-resources-from-pmr.sentinel"
    enforcement_level = "soft-mandatory"
}

